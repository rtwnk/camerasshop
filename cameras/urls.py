from django.conf.urls import include, url

import views

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^(?P<itemid>.+)/order$', views.orderitem, name='orderitem'),
]
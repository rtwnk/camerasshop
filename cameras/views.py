from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import loader
from django.core.exceptions import ObjectDoesNotExist
from models import *
from time import gmtime, strftime
from forms import *

def main(request):
    if request.method == 'POST':
        if 'searchbtn' in request.POST:
            template = loader.get_template('cameras/main.html')
            cred = SearchByCredentialsForm()
            context = {'routelist': getallitems(request.POST['credentials']), 'credform': cred}
            return HttpResponse(template.render(context, request))
    template = loader.get_template('cameras/main.html')
    cred = SearchByCredentialsForm()
    context = {'routelist': getallitems(""), 'credform': cred}
    return HttpResponse(template.render(context, request))

def orderitem(request, itemid):
    if request.method == 'POST':
        if 'orderitem' in request.POST:
            name = request.POST['name']
            surname = request.POST['surname']
            email = request.POST['email']
            camera = Camera.objects.get(id=itemid)
            Order.objects.create(name=name, surname = surname,email=email, camera=camera)
            return HttpResponseRedirect('/cameras')
    print Camera.objects.get(id=itemid)
    context = {'item':Camera.objects.get(id=itemid),'orderform':OrderForm()}
    template = loader.get_template('cameras/order_item.html')
    return HttpResponse(template.render(context, request))

def getallitems(credentials):
    if not credentials:
        items = [x for x in Camera.objects.all()]
        listitems = [list(x) for x in Camera.objects.all().values_list()]
    else:
        items = [x for x in Camera.objects.filter(title__search=credentials)]
        listitems = [list(x) for x in Camera.objects.filter(title__search=credentials).values_list()]
    for i in xrange(len(listitems)):
        features = [j for j in items[i].features.all().values_list()]
        listitems[i].append(features)
    return listitems
    

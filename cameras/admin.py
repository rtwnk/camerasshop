from django.contrib import admin

from .models import Feature, Camera, Order

admin.site.register(Feature)
admin.site.register(Camera)

admin.site.register(Order)
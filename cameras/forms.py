from django import forms

class OrderForm(forms.Form):
    name = forms.CharField(label='Enter your name', max_length=50)
    surname = forms.CharField(label='Enter your surname', max_length=50)
    email = forms.CharField(label='Enter your email', max_length=50)


class SearchByCredentialsForm(forms.Form):
    credentials = forms.CharField(label='Input searching key', max_length=50)
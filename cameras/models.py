from __future__ import unicode_literals
from django.db import models

class Feature(models.Model):
    name = models.CharField(max_length=50, default = "")  

    def __str__(self):
        return self.name

class Camera(models.Model):
    price = models.CharField(max_length=100, default = "")
    title = models.CharField(max_length=100, default = "")
    features = models.ManyToManyField(Feature) 
    zoom = models.CharField(max_length=100, default = "") 
    resolution = models.CharField(max_length=100, default = "")
    image = models.CharField(max_length=100, default = "")

    def __str__(self):
        return str(self.price) + " " + self.title
    
    def __list__(self):
        return [self.price,self.title,self.features,self.zoom,self.resolution,self.image]
        
class Order(models.Model):
    name = models.CharField(max_length=100, default = "")
    surname = models.CharField(max_length=100, default = "")
    email = models.CharField(max_length=100, default = "") 
    camera = models.ForeignKey(Camera,on_delete=models.CASCADE, default = None)

    def __str__(self):
        return self.name + " " + self.surname + " "

